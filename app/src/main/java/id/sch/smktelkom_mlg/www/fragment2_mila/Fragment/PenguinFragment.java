package id.sch.smktelkom_mlg.www.fragment2_mila.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.sch.smktelkom_mlg.www.fragment2_mila.R;

/**
 * Created by Mila on 3/1/2018.
 */

public class PenguinFragment extends Fragment {
    public PenguinFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.activity_multiple_penguin, container, false);
        return rootView;
    }
}

